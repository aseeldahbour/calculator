from Core.MainFunctions import MainFunctions
from Core.calculate import Calculator


class TestCalculator():
    def test_calculator(self,file_name):
        test_inst = TestCalculator()
        calc_inst = Calculator()
        operations = MainFunctions.readFile(file_name)
        for operation in operations:
            if '+' in operation:
                #1+1=2 => a = 1, b=1, c=2
                a, b, c = test_inst.get_values_from_line(operation, '+')
                value= calc_inst.addition(a,b)
                print(value == c )
            elif '-' in operation:
                a, b, c = test_inst.get_values_from_line(operation, '-')
                value = calc_inst.subtraction(a, b)
                print(value == c)
            elif '*' in operation:
                a, b, c = test_inst.get_values_from_line(operation, '*')
                value = calc_inst.multiplication(a, b)
                print(value == c)
            elif '/' in operation:
                a, b, c = test_inst.get_values_from_line(operation, '/')
                value = calc_inst.division(a, b)
                print(value == c)

    def get_values_from_line(self,line, oper):
        first_list = line.split(oper)  # ['1','']
        a = first_list[0]
        b = first_list[1].split('=')[0]
        c = first_list[1].split('=')[1].split('\n')[0]
        return int(a),int(b),int(c)

if __name__ == '__main__':
    test_inst = TestCalculator()
    test_inst.test_calculator("TestCalculator")
