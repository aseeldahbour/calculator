import unittest

from ddt import ddt, unpack, data
import allure

from csvfile.Course import Course
from csvfile.Teacher import Teacher
from csvfile.university_system.Read import Read
from csvfile.university_system.Student import Student


@ddt
class TestUniversity(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.students_list = []
        cls.teacher_list = []
        cls.course_list = []

        print("setup class")

    @data(*Read.read_file("./csvfile/university_system/Students.csv"))
    @unpack
    def test_create_students(self, Student_name, major, id):
        with allure.step('create students'):
            student_info = Student(Student_name, major, id)
            self.students_list.append(student_info)
            print("test_create_students")

    @data(*Read.read_file("./csvfile/university_system/Teachers.csv"))
    @unpack
    def test_create_teachers(self, Teacher_name, id):
        with allure.step('create teachers'):
            teacher_info = Teacher(Teacher_name, id)
            self.teacher_list.append(teacher_info)
            print("test_create_teachers")

    @data(*Read.read_file("./csvfile/university_system/Courses.csv"))
    @unpack
    def test_create_courses(self, name, number_of_hours):
        with allure.step('create courses'):
            course_info = Course(name, number_of_hours)
            self.course_list.append(course_info)
            print("test_create_courses")

    @data(*Read.read_file("./csvfile/university_system/StudentReg.csv"))
    @unpack
    def test_register_courses(self, Student_id, Course_name):
        with allure.step('register courses'):
            student_inst = Read.get_student_by_id(Student_id, self.students_list)
            course_inst = Read.get_course_by_name(Course_name, self.course_list)
            student_inst.register(course_inst)
            print("test_register_courses")

    @data(*Read.read_file("./csvfile/university_system/CourseInfo.csv"))
    @unpack
    def test_teacher_assignment(self, Course_name, Teacher_id):
        with allure.step('assign teachers to courses'):
            teacher_inst = Read.get_teacher_by_id(Teacher_id, self.teacher_list)
            course_inst = Read.get_course_by_name(Course_name, self.course_list)
            teacher_inst.assign_course(course_inst)
            print("test_teacher_assignment")

    @data(*Read.read_file("./csvfile/university_system/StudentReg.csv"))
    @unpack
    def test_validate_reg(self, Student_id, Course_name):
        with allure.step('validate registration'):
            student_inst = Read.get_student_by_id(Student_id, self.students_list)
            course_inst = Read.get_course_by_name(Course_name, self.course_list)
            course_inst.set_student(student_inst)
            self.assertEqual(course_inst.get_student_id(), Student_id)
            courses_reg = student_inst.get_registered_courses()
            allure.attach(str(Course_name + ' ' + Student_id), "pass.txt", allure.attachment_type.TEXT)
            self.assertTrue(Course_name in courses_reg,
                            "course " + str(Course_name) + " not exist in " + str(courses_reg))
            print("test_validate_data")

    @data(*Read.read_file("./csvfile/university_system/CourseInfo.csv"))
    @unpack
    def test_validate_teachers(self, Course_name, Teacher_id):
        with allure.step('validate teachers'):
            teacher_inst = Read.get_teacher_by_id(Teacher_id, self.teacher_list)
            course_inst = Read.get_course_by_name(Course_name, self.course_list)
            self.assertEqual(course_inst.get_teacher_id(), Teacher_id)
            courses = teacher_inst.get_courses()
            allure.attach(str(Course_name + ' ' + Teacher_id), "pass.txt", allure.attachment_type.TEXT)
            self.assertTrue(Course_name in courses, "course " + str(Course_name) + " not exist in " + str(courses))
            print("test_teacher_assignment")

    @classmethod
    def tearDownClass(cls):
        print("tear down")
