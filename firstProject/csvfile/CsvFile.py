
from Core.GetReq import GetReq


class CsvFile:

    @staticmethod
    def get_result(path):
        request_method = GetReq()

        url = "https://jsonplaceholder.typicode.com"
        request_path = url + path
        request_result = request_method.get_request(request_path)

        return request_result

    @staticmethod
    def check_response(request_result, test_name, expected_res_code, expected_res, errors):


        if int(expected_res_code) != request_result.status_code:
            errors.append(
                "in " + test_name + " expected status code is " + str(expected_res_code) + " and actual is " + str(
                    request_result.status_code))
        if expected_res not in request_result.text:
            errors.append("in " + test_name + " expected response must contains " + expected_res + " and it's " + request_result.text)

        return errors


