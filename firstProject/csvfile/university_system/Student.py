class Student:
    def __init__(self, name, major, student_id):
        self.name = name
        self.major = major
        self.id = student_id
        self.courses = []

    def get_student_info(self):
        student_dict = {"name": self.name, "id": self.id, "major": self.major, "list_courses": self.courses}
        return student_dict

    def register(self, course):
        self.courses.append(course)

    def get_registered_courses(self):
        course_names = []
        for course in self.courses:
            student_dict = course.get_course_info()
            name = student_dict["name"]

            course_names.append(name)
        return course_names


