import csv


class Read:
    @staticmethod
    def read_file(filename):
        rows = []
        csv_data = open(str(filename), "r", encoding="utf8")
        content = csv.reader(csv_data)
        next(content, None)
        for row in content:
            rows.append(row)
        return rows


    @staticmethod
    def get_student_by_id(student_id, students_list):
        for student in students_list:
            student_info = student.get_student_info()
            if student_info["id"] == student_id:
                return student

    @staticmethod
    def get_course_by_name(course_name, course_list):
        for course in course_list:
            course_info = course.get_course_info()
            if course_info["name"] == course_name:
                return course

    @staticmethod
    def get_teacher_by_id(teacher_id, teacher_list):
        for teacher in teacher_list:
            teacher_info = teacher.get_teacher_info()
            if teacher_info["id"] == teacher_id:
                return teacher
