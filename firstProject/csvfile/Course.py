class Course:
    def __init__(self, name, number_of_hourse):
        self.name = name
        self.number_of_hourse = number_of_hourse
        self.teacher_name = None
        self.students = []
        self.teacher_id = None
        self.student_name = None
        self.student_id = None

    def get_course_info(self):
        course_dict = {"name": self.name, "number_of_hourse": self.number_of_hourse}
        return course_dict

    def get_student(self):
        return self.students

    def get_teacher_name(self):
        return self.teacher_name

    def get_teacher_id(self):
        return self.teacher_id

    # student register
    def student_register(self, student):
        self.students.append(student)

    # define teacher
    def set_teacher(self, teacher):
        info = teacher.get_teacher_info()
        self.teacher_name = info['name']
        self.teacher_id = info['id']

    def set_student(self, student):
        info = student.get_student_info()
        self.student_name = info['name']
        self.student_id = info['id']

    def get_student_id(self):
        return self.student_id

