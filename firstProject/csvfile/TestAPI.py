import unittest
import allure
from ddt import ddt, data, unpack

from csvfile.CsvFile import CsvFile
from csvfile.university_system.Read import Read


@ddt
class TestAPI(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("setup class")

    @data(*Read.read_file("API_Test.csv"))
    @unpack
    def test_method(self, Name, path, response_code, response):
        with allure.step('test method'):
            errors = []
            print(str(Name))
            request_result = CsvFile.get_result(path)
            CsvFile.check_response(request_result, Name, response_code, response, errors)
            assert len(errors) == 0, str(errors)

    @classmethod
    def tearDownClass(cls):
        print("tear down")
