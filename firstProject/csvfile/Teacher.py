
class Teacher:
    def __init__(self, name, teacher_id):
        self.name = name
        self.id = teacher_id
        self.courses = []

    def get_teacher_info(self):
        teacher_dict = {"name": self.name, "id": self.id}
        return teacher_dict

    def get_courses(self):
        course_names = []
        for course in self.courses:
            teacher_dict = course.get_course_info()
            name = teacher_dict["name"]

            course_names.append(name)
        return course_names

    def set_courses(self, course_list):
        self.courses = course_list

    def assign_course(self, course):
        self.courses.append(course)
        course.set_teacher(self)


