import time

from selenium import webdriver


class Selenium:
    @staticmethod
    def open_chrom():
        driver = webdriver.Chrome('../Core/webdriver/chromedriver.exe')
        page = driver.get("https://www.bing.com/")
        time.sleep(10)
        driver.close()
        return page


if __name__ == '__main__':
    Selenium.open_chrom()
