
from csvfile.Course import Course
from csvfile.Teacher import Teacher
from csvfile.university_system.Read import Read

from csvfile.university_system.Student import Student

if __name__ == '__main__':

    teacher_data = Read.read_file("university_system/Teachers.csv")
    courses_info = Read.read_file("university_system/CourseInfo.csv")
    student_reg = Read.read_file("university_system/StudentReg.csv")
    courses_data = Read.read_file("university_system/Courses.csv")
    student_data = Read.read_file("university_system/Students.csv")

    student_info_list = []
    course_info_list = []
    teacher_info_list = []

    for row in student_data:
        if row[0] != "Student_name" and row[2] != "id" and row[1] != "major":
            student_name = row[0]
            student_id = row[2]
            student_major = row[1]
            student_info = Student(student_name, student_id, student_major)
            student_info_list.append(student_info)
    # print(student_info_list)

    for row in courses_data:
        if row[0] != "name" and row[1] != "number of hours":
            course_name = row[0]
            num_of_hours = row[1]
            course_info = Course(course_name, num_of_hours)
            course_info_list.append(course_info)
    # print(course_info_list)

    for row in teacher_data:
        if row[0] != "Teacher_name" and row[1] != "id":
            teacher_name = row[0]
            teacher_id = row[1]
            teacher_info = Teacher(teacher_name, teacher_id)
            teacher_info_list.append(teacher_info)
    # print(teacher_info_list)

    for row in courses_info:
        if row[0] != "Course_name" and row[1] != "Teacher_id":
            course_name = row[0]
            teacher_id = row[1]
            teacher_inst = Read.get_teacher_by_id(teacher_id , teacher_info_list)
            course_inst = Read.get_course_by_name(course_name, course_info_list)
            teacher_inst.assign_course(course_inst)
            print(teacher_inst.get_teacher_info())
            print(teacher_inst.get_courses())

    for row in student_reg:
        if row[0] != "Student_id" and row[1] != "Course_name":
            student_id = row[0]
            course = row[1]
            student_inst = Read.get_student_by_id(student_id, student_info_list)
            course_inst = Read.get_course_by_name(course, course_info_list)
            student_inst.register(course_inst)
            #print(student_inst.get_registered_courses())
