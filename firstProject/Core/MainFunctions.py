class MainFunctions:
    @staticmethod
    def writeFile(file_path, data):
        f = open(file_path, "w")

        for line in data:
            f.write (line)
            f.write('\n')

        f.close()

    @staticmethod
    def readFile(fileName):
        f = open(fileName, "r")
        lines = f.readlines()
        f.close()
        return lines

if __name__ == '__main__':
    data = ['Rasha', 'Aseel', 'Ahmad', 'test']
    MainFinctions.writeFile("testFile.txt",data)
    print(MainFinctions.readFile("testFile.txt"))