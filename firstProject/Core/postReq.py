import requests


class PostReq:

    def post_request(self, url, data):
        r = requests.post(url, data)
        return r


if __name__ == '__main__':
    post_inst = PostReq()
    url = "https://jsonplaceholder.typicode.com/posts"
    body = {"userId": 1, "title": "test_title", "body": "test_body"}
    response = post_inst.post_request(url,
                                      body)
    print(response.status_code)
    print(response.json())

