import requests


class GetReq:

    def get_request(self, url):
        r = requests.get(url)
        return r


if __name__ == '__main__':
    get_inst = GetReq()
    url = "https://jsonplaceholder.typicode.com/posts/1"
    response = get_inst.get_request(url)
    print(response.status_code)
    print(response.text)

