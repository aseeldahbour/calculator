import requests


class PutReq:

    def put_request(self, url, data):
        r = requests.post(url, data)
        return r


if __name__ == '__main__':
    put_inst = PutReq()
    response = put_inst.put_request("https://jsonplaceholder.typicode.com/posts/1",
                                      {"userId": 1, "title": "test_title", "body": "test_body"})
    print(response.status_code)

    print(response.content)
